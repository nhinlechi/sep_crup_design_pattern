﻿using System;
using System.Collections.Generic;
using CustomHibernateSQL.DBInterface;
using System.Reflection;
using System.Linq;
using CustomHibernateSQL.SQLServer;

namespace CustomHibernateSQL.DAO
{
    public abstract class BaseDAO<T>
    {
        protected IDatabase db = new SQLDatabase();
        private Type type = typeof(T);
        public List<T> GetAll()
        {
            return db.GetAll(type).Cast<T>().ToList();   
        }

        public void Update(T obj)
        {
            db.Update(obj);
        }

        public void Delete(T obj)
        {
            db.Delete(obj);
        }

        public void Insert(T obj)
        {
            db.Insert(obj);
        }
    }
}
