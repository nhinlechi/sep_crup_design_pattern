﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomHibernateSQL.DBInterface
{
    public interface IDatabase
    {
        // Main Methods
        IList GetAll(Type type);
        int Update(object obj);
        int Insert(object obj);
        int Delete(object obj);
    }
}
